package com.apolorossi.marketplaceapp.rest.manager;

import android.content.Context;

import com.apolorossi.marketplaceapp.model.MarketModel;
import com.apolorossi.marketplaceapp.rest.RetrofitClientInstance;
import com.apolorossi.marketplaceapp.rest.model.MarketRestModel;
import com.apolorossi.marketplaceapp.rest.service.RestService;

import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarketManager {

    private static MarketManager sInstance;

    public MarketManager(){

    }

    public static MarketManager with(Context context){
        if (sInstance == null){
            return new MarketManager();
        }
        return sInstance;
    }

    public void fetch(final OnMarketCallback callback){
        RestService restService = RetrofitClientInstance.
                getRetrofitInstance().create(RestService.class);

        Call<List<MarketRestModel>> call = restService.getMarkets();

        call.enqueue(new Callback<List<MarketRestModel>>() {
            @Override
            public void onResponse(Call<List<MarketRestModel>> call, Response<List<MarketRestModel>> response) {
                callback.onSuccess(restToModel(response));
            }

            @Override
            public void onFailure(Call<List<MarketRestModel>> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    public List<MarketModel> restToModel(Response<List<MarketRestModel>> response){
        List<MarketModel> list = new LinkedList<>();

        for ( MarketRestModel market : response.body()){
            list.add(market.restToModel());
        }

        return list;
    }

    public interface OnMarketCallback{
        void onSuccess(List<MarketModel> marketList);
        void onFailure();
    }

}
