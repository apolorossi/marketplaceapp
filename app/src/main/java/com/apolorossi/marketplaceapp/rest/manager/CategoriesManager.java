package com.apolorossi.marketplaceapp.rest.manager;

import android.content.Context;

import com.apolorossi.marketplaceapp.model.CategoryModel;
import com.apolorossi.marketplaceapp.model.MarketModel;
import com.apolorossi.marketplaceapp.rest.RetrofitClientInstance;
import com.apolorossi.marketplaceapp.rest.model.CategoryRestModel;
import com.apolorossi.marketplaceapp.rest.model.MarketRestModel;
import com.apolorossi.marketplaceapp.rest.service.RestService;

import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriesManager {

    private static MarketManager sInstance;

    public static MarketManager with(Context context) {
        if (sInstance == null){
            return sInstance = new MarketManager();
        }
        return sInstance;
    }

    public void fetch(final OnCategoryCallback callback, int id){
        RestService restService = RetrofitClientInstance.
                getRetrofitInstance().create(RestService.class);

        Call<List<CategoryRestModel>> call = restService.getCategories(id);

        call.enqueue(new Callback<List<CategoryRestModel>>() {

            @Override
            public void onResponse(Call<List<CategoryRestModel>> call, Response<List<CategoryRestModel>> response) {
                callback.onSuccess(restToModel(response));
            }

            @Override
            public void onFailure(Call<List<CategoryRestModel>> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    public List<CategoryModel> restToModel(Response<List<CategoryRestModel>> categoryList){
        List<CategoryModel> list = new LinkedList<>();

        for (CategoryRestModel category : categoryList.body()){
            list.add(category.restToModel());
        }

        return list;

    }

    public interface OnCategoryCallback{
        void onSuccess(List<CategoryModel> categoriesList);
        void onFailure();
    }
}
