package com.apolorossi.marketplaceapp.rest.service;

import com.apolorossi.marketplaceapp.rest.model.CategoryRestModel;
import com.apolorossi.marketplaceapp.rest.model.MarketRestModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestService {

    @GET("/markets")
    Call<List<MarketRestModel>> getMarkets();

    @GET("/market/{id}/category")
    Call<List<CategoryRestModel>> getCategories(@Path("id") int marketCode);

    @GET("/market/{marketId}/category/{categoryId}/products")
    Call<List<MarketRestModel>> getProducs();

    @GET("/market/{id}/bestDeals")
    Call<List<MarketRestModel>> getBestDeals();

}