package com.apolorossi.marketplaceapp.rest.model;

import java.util.List;

public class BestPriceProductsRestListModel {

    private List<ProductRestModel> mProductList;

    public List<ProductRestModel> getProductList() {
        return mProductList;
    }

    public void setProductList(List<ProductRestModel> mProductList) {
        this.mProductList = mProductList;
    }

}
