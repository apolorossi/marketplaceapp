package com.apolorossi.marketplaceapp.rest.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.apolorossi.marketplaceapp.model.MarketModel;
import com.google.gson.annotations.SerializedName;

import java.sql.Blob;
import java.sql.SQLException;

public class MarketRestModel {

    @SerializedName("marketId")
    private String marketId;

    @SerializedName("marketName")
    private String marketName;

    @SerializedName("cnpj")
    private String cnpj;

    @SerializedName("address")
    private String address;

    @SerializedName("email")
    private String email;

    @SerializedName("contact")
    private String contact;

    @SerializedName("logoUrl")
    private String logo;

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLogo() {

        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public MarketModel restToModel(){
        return new MarketModel(
                marketId, marketName, cnpj, address, email, contact, getLogo());
    }

}
