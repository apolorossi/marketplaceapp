package com.apolorossi.marketplaceapp.rest.model;

import com.apolorossi.marketplaceapp.model.CategoryModel;
import com.google.gson.annotations.SerializedName;

public class CategoryRestModel {

    @SerializedName("categoryName")
    private String categoryName;

    @SerializedName("imageUrl")
    private String imageUrl;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public CategoryModel restToModel(){
        return new CategoryModel(categoryName, imageUrl);

    }
}
