package com.apolorossi.marketplaceapp.model;

import java.io.Serializable;
import java.util.List;

public class OrderModel implements Serializable {

    private MarketModel market;

    private List<ProductModel> mProductList;

    private Double mTotalValue;

    public MarketModel getMarket() {
        return market;
    }

    public void setMarket(MarketModel market) {
        this.market = market;
    }

    public List<ProductModel> getmProductList() {
        return mProductList;
    }

    public void setProductList(List<ProductModel> mProductList) {
        this.mProductList = mProductList;
    }

    public Double getmTotalValue() {
        return mTotalValue;
    }

    public void setmTotalValue(Double mTotalValue) {
        this.mTotalValue = mTotalValue;
    }
}
