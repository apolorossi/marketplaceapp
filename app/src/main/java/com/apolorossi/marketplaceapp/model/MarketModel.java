package com.apolorossi.marketplaceapp.model;

import android.graphics.Bitmap;

import java.io.Serializable;

public class MarketModel implements Serializable {

    private String marketId;
    private String market_name;
    private String cnpj;
    private String address;
    private String email;
    private String contact;
    private String logo;

    public MarketModel(){

    }

    public MarketModel(String marketId, String market_name, String cnpj, String address, String email, String contact, String logo) {
        this.marketId = marketId;
        this.market_name = market_name;
        this.cnpj = cnpj;
        this.address = address;
        this.email = email;
        this.contact = contact;
        this.logo = logo;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public String getMarket_name() {
        return market_name;
    }

    public void setMarketName(String market_name) {
        this.market_name = market_name;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


}
