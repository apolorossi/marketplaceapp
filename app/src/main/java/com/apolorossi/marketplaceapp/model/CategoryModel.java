package com.apolorossi.marketplaceapp.model;

import java.io.Serializable;

public class CategoryModel implements Serializable {

    private String categoryName;
    private String imageUrl;

    public CategoryModel(String category, String image){
        categoryName = category;
        imageUrl = image;
    }

    public CategoryModel(){

    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
