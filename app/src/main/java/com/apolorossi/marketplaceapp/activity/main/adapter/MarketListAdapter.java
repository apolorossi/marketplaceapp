package com.apolorossi.marketplaceapp.activity.main.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apolorossi.marketplaceapp.R;
import com.apolorossi.marketplaceapp.activity.main.MainActivity;
import com.apolorossi.marketplaceapp.model.MarketModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MarketListAdapter extends Adapter<MarketViewHolder>{

    private List<MarketModel> mMarketList;
    private MarketViewHolder mViewHolder;
    private MainActivity.OnItemClickListener mOnItemClickListener;
    private Picasso mPicasso;

    public MarketListAdapter(List<MarketModel> marketList,
                             MainActivity.OnItemClickListener onItemClickListener,
                             Picasso picasso){
        mMarketList = marketList;
        mOnItemClickListener = onItemClickListener;
        mPicasso = picasso;
    }

    @Override
    public MarketViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.market_view_holder, parent, false);
        mViewHolder = new MarketViewHolder(view);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MarketViewHolder holder, final int position) {
//        holder.logo.setImageBitmap(mMarketList.get(position).getLogo());
        mPicasso.load(
                mMarketList.get(position).getLogo()).into(holder.logo);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(mMarketList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMarketList.size();
    }


}
