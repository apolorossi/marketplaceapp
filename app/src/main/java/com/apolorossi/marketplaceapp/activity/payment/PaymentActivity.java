package com.apolorossi.marketplaceapp.activity.payment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.apolorossi.marketplaceapp.R;

import butterknife.OnClick;

public class PaymentActivity extends AppCompatActivity {


    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, PaymentActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
    }


    @OnClick(R.id.activity_payment_confirm_button)
    public void onConfirmButtonClick(){
        Toast.makeText(getBaseContext(),"Olaa", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
    }
}
