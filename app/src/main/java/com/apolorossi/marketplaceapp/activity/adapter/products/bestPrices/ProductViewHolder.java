package com.apolorossi.marketplaceapp.activity.adapter.products.bestPrices;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.apolorossi.marketplaceapp.R;

public class ProductViewHolder extends RecyclerView.ViewHolder {

    public TextView productName;
    public CardView productHolder;

    public ProductViewHolder(View view) {
        super(view);
        productName = view.findViewById(R.id.product_name);
        productHolder = view.findViewById(R.id.product_holder);
    }
}
