package com.apolorossi.marketplaceapp.activity.categories.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apolorossi.marketplaceapp.R;
import com.apolorossi.marketplaceapp.activity.categories.CategoriesActivity;
import com.apolorossi.marketplaceapp.model.CategoryModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoriesListAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private List<CategoryModel> mCategoriesList;
    private CategoryViewHolder mViewHolder;
    private CategoriesActivity.OnCategoryItemClickListener mOnItemClickListener;
    private Picasso mPicasso;

    public CategoriesListAdapter(List<CategoryModel> categoriesList,
                                 CategoriesActivity.OnCategoryItemClickListener itemClickListener,
                                 Picasso picasso){
        mPicasso = picasso;
        mCategoriesList = categoriesList;
        mOnItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_view_holder, parent, false);
        mViewHolder = new CategoryViewHolder(view);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, final int position) {
        holder.categoryName.setText(mCategoriesList.get(position).getCategoryName());
        mPicasso.
                load(mCategoriesList.get(position).getImageUrl())
                .into(holder.categoryIcon);

        holder.mHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(mCategoriesList.get(position));
            }
        });
    }


    @Override
    public int getItemCount() {
        return mCategoriesList.size();
    }
}
