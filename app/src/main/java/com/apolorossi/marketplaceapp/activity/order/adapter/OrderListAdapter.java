package com.apolorossi.marketplaceapp.activity.order.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apolorossi.marketplaceapp.R;
import com.apolorossi.marketplaceapp.activity.main.MainActivity;
import com.apolorossi.marketplaceapp.activity.order.CloseOrderActivity;
import com.apolorossi.marketplaceapp.model.MarketModel;
import com.apolorossi.marketplaceapp.model.ProductModel;

import java.util.List;

public class OrderListAdapter extends Adapter<OrderViewHolder> {

    private List<ProductModel> mProductList;
    private OrderViewHolder mViewHolder;
    private CloseOrderActivity.OnItemClickListener mOnItemClickListener;

    public OrderListAdapter(List<ProductModel> productList, CloseOrderActivity.OnItemClickListener onItemClickListener){
        mProductList = productList;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_view_holder, parent, false);
        mViewHolder = new OrderViewHolder(view);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderViewHolder holder, final int position) {
//        holder.logo.setImageBitmap(mMarketList.get(position).getLogo());
      /*  holder.name.setText("");
        holder.quantity.setText("");
        holder.value.setText("");*/

      holder.orderHolder.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              mOnItemClickListener.onItemClickListener(mProductList.get(position), position);
          }
      });


    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }


}
