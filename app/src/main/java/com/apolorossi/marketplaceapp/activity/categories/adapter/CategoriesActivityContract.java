package com.apolorossi.marketplaceapp.activity.categories.adapter;

import com.apolorossi.marketplaceapp.model.CategoryModel;

import java.util.List;

public interface CategoriesActivityContract {

    interface IView {
        void onFetchList(List<CategoryModel> category);
    }

    interface IPresenter {
        void onViewReady(String id);
    }
}
