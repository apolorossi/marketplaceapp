package com.apolorossi.marketplaceapp.activity.adapter.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apolorossi.marketplaceapp.R;
import com.apolorossi.marketplaceapp.activity.categories.CategoriesActivity;
import com.apolorossi.marketplaceapp.model.ProductModel;

import java.util.List;

public class ProductVerticalListAdapter extends RecyclerView.Adapter<ProductVerticalViewHolder> {

    private List<ProductModel> mProductsList;
    private ProductVerticalViewHolder mViewHolder;
    private CategoriesActivity.OnProductItemClickListener mOnClickListener;

    public ProductVerticalListAdapter(Context context,
                                      List<ProductModel> productList,
                                      CategoriesActivity.OnProductItemClickListener onProductItemClickListener){
        mProductsList = productList;
        mOnClickListener = onProductItemClickListener;
    }

    @NonNull
    @Override
    public ProductVerticalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_vertical_view_holder, parent, false);

        mViewHolder = new ProductVerticalViewHolder(view);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductVerticalViewHolder holder, final int position) {
        holder.productName.setText(mProductsList.get(position).getProductName());
       /* holder.productCurrentValue.setText("");
        holder.productNormalValue.setText("");
        holder.productExpiration.setText("");*/
        holder.productIcon.setImageResource(R.mipmap.logo_mercado);

        holder.productHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnClickListener.onItemClick(mProductsList.get(position));
            }
        });

    }


    @Override
    public int getItemCount() {
        return mProductsList.size();
    }
}
