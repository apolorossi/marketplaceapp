package com.apolorossi.marketplaceapp.activity.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.apolorossi.marketplaceapp.R;
import com.apolorossi.marketplaceapp.activity.categories.CategoriesActivity;
import com.apolorossi.marketplaceapp.activity.main.adapter.MarketListAdapter;
import com.apolorossi.marketplaceapp.model.MarketModel;
import com.apolorossi.marketplaceapp.rest.RetrofitClientInstance;
import com.apolorossi.marketplaceapp.rest.manager.MarketManager;
import com.apolorossi.marketplaceapp.rest.model.MarketRestModel;
import com.apolorossi.marketplaceapp.rest.service.RestService;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MainActivityContract.IView {

    @BindView(R.id.main_activity_market_list)
    protected RecyclerView mMarketList;

    protected MainActivityContract.IPresenter mPresenter;

    List<MarketModel> mMarketModelList = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mPresenter = new MainActivityPresenter(this);

        setupMarketList();

        mPresenter.onViewReady(getBaseContext());
    }

    public void receiveMarkets(List<MarketModel> marketList){
        mMarketModelList = marketList;
        setupMarketList();
    }

    private void setupMarketList() {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        mMarketList.setLayoutManager(layoutManager);

        MarketListAdapter marketListAdapter = new MarketListAdapter(mMarketModelList, onItemClickListener, Picasso.with(getBaseContext()));
        mMarketList.setAdapter(marketListAdapter);
    }

    protected OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(MarketModel market) {
            startActivity(
                    CategoriesActivity.getStartIntent(getBaseContext(), market));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        }
    };

    public interface OnItemClickListener {
        void onItemClick(MarketModel market);
    }
}
