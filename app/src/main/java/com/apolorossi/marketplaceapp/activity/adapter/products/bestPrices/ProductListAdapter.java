package com.apolorossi.marketplaceapp.activity.adapter.products.bestPrices;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apolorossi.marketplaceapp.R;
import com.apolorossi.marketplaceapp.activity.categories.CategoriesActivity;
import com.apolorossi.marketplaceapp.model.ProductModel;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductViewHolder> {

    private List<ProductModel> mProductsList;
    private ProductViewHolder mViewHolder;
    private CategoriesActivity.OnProductItemClickListener mOnClickListener;

    public ProductListAdapter(Context context,
                              List<ProductModel> productList,
                              CategoriesActivity.OnProductItemClickListener onProductItemClickListener){
        mProductsList = productList;
        mOnClickListener = onProductItemClickListener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_view_holder, parent, false);

        mViewHolder = new ProductViewHolder(view);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, final int position) {
            holder.productName.setText(mProductsList.get(position).getProductName());
            holder.productName.setText(mProductsList.get(position).getProductName());
            holder.productHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnClickListener.onItemClick(mProductsList.get(position));
                }
            });
    }


    @Override
    public int getItemCount() {
        return mProductsList.size();
    }
}
