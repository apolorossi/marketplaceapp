package com.apolorossi.marketplaceapp.activity.main.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.apolorossi.marketplaceapp.R;

public class MarketViewHolder extends RecyclerView.ViewHolder {

    public ImageView logo;
    public CardView cardView;


    public MarketViewHolder(View view) {
        super(view);
        logo =  view.findViewById(R.id.holder_logo);
        cardView = view.findViewById(R.id.market_holder_card);
    }


}
