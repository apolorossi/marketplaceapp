package com.apolorossi.marketplaceapp.activity.categories.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apolorossi.marketplaceapp.R;

public class CategoryViewHolder extends RecyclerView.ViewHolder {

    public TextView categoryName;
    public ImageView categoryIcon;
    public ConstraintLayout mHolder;

    public CategoryViewHolder(View view) {
        super(view);
        categoryName = view.findViewById(R.id.category_name);
        categoryIcon = view.findViewById(R.id.category_icon);
        mHolder = view.findViewById(R.id.category_holder);
    }
}
