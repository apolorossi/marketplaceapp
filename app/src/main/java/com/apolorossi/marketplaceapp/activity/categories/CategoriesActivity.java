package com.apolorossi.marketplaceapp.activity.categories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.apolorossi.marketplaceapp.R;
import com.apolorossi.marketplaceapp.activity.categories.adapter.CategoriesActivityContract;
import com.apolorossi.marketplaceapp.activity.categories.adapter.CategoriesListAdapter;
import com.apolorossi.marketplaceapp.activity.adapter.products.bestPrices.ProductListAdapter;
import com.apolorossi.marketplaceapp.activity.products.ProductsActivity;
import com.apolorossi.marketplaceapp.model.CategoryModel;
import com.apolorossi.marketplaceapp.model.MarketModel;
import com.apolorossi.marketplaceapp.model.ProductModel;
import com.apolorossi.marketplaceapp.rest.manager.CategoriesManager;
import com.apolorossi.marketplaceapp.rest.model.CategoryRestModel;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesActivity extends AppCompatActivity implements CategoriesActivityContract.IView {

    private static String EXTRA = "extra";
    private static String EXTRA_MARKET = "extra_market";

    @BindView(R.id.best_prices_list)
    protected RecyclerView mBestProductsRecycler;

    @BindView(R.id.categories_list)
    protected RecyclerView mCategoriesRecycler;

    private MarketModel mMarket;

    List<CategoryModel> mCategoryList = new LinkedList<>();

    CategoriesActivityContract.IPresenter mPresenter;

    private OnCategoryItemClickListener onItemClickListener = new OnCategoryItemClickListener() {
        @Override
        public void onItemClick(CategoryModel category) {

            startActivity(
                    ProductsActivity.getStartIntent(getBaseContext(), category, mMarket));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        }
    };

    private OnProductItemClickListener onProductItemClickListener = new OnProductItemClickListener() {
        @Override
        public void onItemClick(ProductModel product) {
            Log.d("","");
        }
    };

    public static Intent getStartIntent(Context context, MarketModel market){
        Intent intent = new Intent(context, CategoriesActivity.class);

        intent.putExtra(EXTRA_MARKET, market);

        return intent;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        ButterKnife.bind(this);

        mPresenter = new CategoriesPresenter(this);

        if (getIntent().getExtras() != null) {
            mMarket = ((MarketModel) getIntent().getExtras().get(EXTRA_MARKET));
        }

        setupCategoriesList();
        setupBestPricesList();

        mPresenter.onViewReady(mMarket.getMarketId());

    }

    private void setupBestPricesList() {
        mBestProductsRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        List<ProductModel>  productList = new LinkedList<>();

        for (int i =0; i < 10; i++){
            ProductModel product = new ProductModel();
            product.setProductName("Leite Jussara " + i);
            productList.add(product);
        }

        ProductListAdapter productListAdapter = new ProductListAdapter(getBaseContext(), productList, onProductItemClickListener);

        mBestProductsRecycler.setAdapter(productListAdapter);
    }

    private void setupCategoriesList() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        mCategoriesRecycler.setLayoutManager(layoutManager);
     /*
        for (int i = 0; i < 10; i ++){
            CategoryModel category = new CategoryModel();
            category.setCategoryName("MERCEARIA");
            category.setImageUrl("https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/carrefour.png?alt=media&token=15a29617-f8c5-451c-93fc-b7e0734884f5");
            categoryList.add(category);
        }*/

        CategoriesListAdapter categoriesListAdapter =
                new CategoriesListAdapter(mCategoryList,
                        onItemClickListener,
                        Picasso.with(getBaseContext()));
        mCategoriesRecycler.setAdapter(categoriesListAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);

    }

    @Override
    public void onFetchList(List<CategoryModel> categoriesList) {
        mCategoryList = categoriesList;
        setupCategoriesList();
    }

    public interface OnCategoryItemClickListener {
        void onItemClick(CategoryModel categoryModel);
    }

    public interface OnProductItemClickListener {
        void onItemClick(ProductModel product);
    }
}
