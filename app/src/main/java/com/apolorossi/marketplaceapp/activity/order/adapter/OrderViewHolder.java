package com.apolorossi.marketplaceapp.activity.order.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.apolorossi.marketplaceapp.R;

public class OrderViewHolder extends RecyclerView.ViewHolder {

    public TextView quantity;
    public TextView name;
    public TextView value;
    public ConstraintLayout orderHolder;


    public OrderViewHolder(View view) {
        super(view);
        quantity = view.findViewById(R.id.order_product_quantity);
        name = view.findViewById(R.id.order_product_name);
        value = view.findViewById(R.id.order_product_value);
        orderHolder = view.findViewById(R.id.order_holder);
    }

}
