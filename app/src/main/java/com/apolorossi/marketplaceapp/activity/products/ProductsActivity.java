package com.apolorossi.marketplaceapp.activity.products;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.apolorossi.marketplaceapp.R;
import com.apolorossi.marketplaceapp.activity.adapter.products.ProductVerticalListAdapter;
import com.apolorossi.marketplaceapp.activity.categories.CategoriesActivity;
import com.apolorossi.marketplaceapp.activity.order.CloseOrderActivity;
import com.apolorossi.marketplaceapp.model.CategoryModel;
import com.apolorossi.marketplaceapp.model.MarketModel;
import com.apolorossi.marketplaceapp.model.OrderModel;
import com.apolorossi.marketplaceapp.model.ProductModel;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductsActivity extends AppCompatActivity {

    public static String EXTRA_CATEGORY = "extra_category";
    public static String EXTRA_MARKET =  "extra_market";

    @BindView(R.id.activity_products_list)
    protected RecyclerView mProductsRecyclerView;

    List<ProductModel> mProductList = new LinkedList<>();

    private CategoryModel mCategory;
    private MarketModel mMarket;

    private OrderModel mOrder;

    private CategoriesActivity.OnProductItemClickListener onProductItemClickListener = new CategoriesActivity.OnProductItemClickListener() {
        @Override
        public void onItemClick(ProductModel product) {
            Log.d("", "");
        }
    };

    public static Intent getStartIntent(Context context, CategoryModel category, MarketModel market) {
        Intent intent = new Intent(context, ProductsActivity.class);
//        intent.putExtra()

        intent.putExtra(EXTRA_CATEGORY, category);
        intent.putExtra(EXTRA_MARKET, market);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            mCategory = ((CategoryModel) getIntent().getExtras().get(EXTRA_CATEGORY));
            mMarket = ((MarketModel) getIntent().getExtras().get(EXTRA_MARKET));
        }

        setupProductList();

        mOrder = new OrderModel();
        mOrder.setMarket(mMarket);
        mOrder.setProductList(mProductList);
        mOrder.setmTotalValue(getTotalValue());

    }

    public double getTotalValue(){
        double total = 0;
        for (ProductModel product : mProductList){
            total += product.getPrice();
        }
        return total;
    }


    @OnClick(R.id.activity_products_checkout)
    public void onCheckoutButtonClick() {
        startActivity(CloseOrderActivity.getStartIntent(getBaseContext(), mOrder));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private void setupProductList() {

        mProductsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        for (int i = 0; i < 10; i++) {
            ProductModel product = new ProductModel();
            product.setProductName("Leite Jussara " + i);
            product.setImageUrl("asasdf");
            product.setPrice(20.0);
            product.setOriginalPrice(30.0);
            mProductList.add(product);
        }

        ProductVerticalListAdapter productListAdapter = new ProductVerticalListAdapter(getBaseContext(), mProductList, onProductItemClickListener);

        mProductsRecyclerView.setAdapter(productListAdapter);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
    }

}
