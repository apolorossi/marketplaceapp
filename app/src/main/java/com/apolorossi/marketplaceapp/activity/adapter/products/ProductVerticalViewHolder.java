package com.apolorossi.marketplaceapp.activity.adapter.products;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apolorossi.marketplaceapp.R;

public class ProductVerticalViewHolder extends RecyclerView.ViewHolder {

    public TextView productName;
    public TextView productCurrentValue;
    public TextView productNormalValue;
    public TextView productExpiration;
    public ImageView productIcon;
    public ConstraintLayout productHolder;


    public ProductVerticalViewHolder(View view) {
        super(view);
        productName = view.findViewById(R.id.product_vertical_product_name);
        productHolder = view.findViewById(R.id.product_vertical_holder);
        productCurrentValue = view.findViewById(R.id.product_vertical_current_value);
        productExpiration = view.findViewById(R.id.product_vertical_expiration);
        productNormalValue = view.findViewById(R.id.product_vertical_normal_value);
        productIcon = view.findViewById(R.id.product_vertical_icon);

    }
}
