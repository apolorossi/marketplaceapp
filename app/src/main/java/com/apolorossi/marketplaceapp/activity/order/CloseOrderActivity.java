package com.apolorossi.marketplaceapp.activity.order;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.apolorossi.marketplaceapp.R;
import com.apolorossi.marketplaceapp.activity.order.adapter.OrderListAdapter;
import com.apolorossi.marketplaceapp.activity.payment.PaymentActivity;
import com.apolorossi.marketplaceapp.model.MarketModel;
import com.apolorossi.marketplaceapp.model.OrderModel;
import com.apolorossi.marketplaceapp.model.ProductModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CloseOrderActivity extends AppCompatActivity {

    public static String ORDER_MODEL;

    @BindView(R.id.activity_close_order_product_list)
    protected RecyclerView mProductRecyclerList;

    private OrderModel mOrder;

    private OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClickListener(ProductModel product, int positon) {

        }
    };

    public static Intent getStartIntent(Context context, OrderModel orderModel){
        Intent intent = new Intent(context, CloseOrderActivity.class);
        intent.putExtra(ORDER_MODEL, orderModel);


        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_order);

        ButterKnife.bind(this);

        if (getIntent().getExtras() != null){
            mOrder = (OrderModel) getIntent().getExtras().get(ORDER_MODEL);
        }

        setupProductRecyclerView();

    }


    @OnClick(R.id.activity_close_order_button)
    public void onCloseOrder(){
        startActivity(PaymentActivity.getIntent(getBaseContext()));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private void setupProductRecyclerView() {
        OrderListAdapter orderListAdapter = new OrderListAdapter(mOrder.getmProductList(), onItemClickListener);
        mProductRecyclerList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mProductRecyclerList.setAdapter(orderListAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
    }

    public interface OnItemClickListener{
        void onItemClickListener(ProductModel product, int positon);
    }
}
