package com.apolorossi.marketplaceapp.activity.main;

import android.content.Context;

import com.apolorossi.marketplaceapp.model.MarketModel;
import com.apolorossi.marketplaceapp.rest.manager.MarketManager;

import java.util.List;

public class MainActivityPresenter implements MainActivityContract.IPresenter{

    private MainActivityContract.IView mView;

    public MainActivityPresenter(MainActivityContract.IView view){
        mView = view;
    }

    public void onViewReady(Context context){
        MarketManager.with(context)
                .fetch(new MarketManager.OnMarketCallback() {
                    @Override
                    public void onSuccess(List<MarketModel> marketList) {
                        mView.receiveMarkets(marketList);
                    }

                    @Override
                    public void onFailure() {

                    }
                });
    }
    
}
