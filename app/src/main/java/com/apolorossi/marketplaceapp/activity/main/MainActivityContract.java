package com.apolorossi.marketplaceapp.activity.main;

import android.content.Context;

import com.apolorossi.marketplaceapp.model.MarketModel;

import java.util.List;

public interface MainActivityContract {

    interface IView{
        void receiveMarkets(List<MarketModel> marketList);
    }

    interface IPresenter{
        void onViewReady(Context context);
    }

}
