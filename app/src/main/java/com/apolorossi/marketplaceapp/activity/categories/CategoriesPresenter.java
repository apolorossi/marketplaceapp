package com.apolorossi.marketplaceapp.activity.categories;

import com.apolorossi.marketplaceapp.activity.categories.adapter.CategoriesActivityContract;
import com.apolorossi.marketplaceapp.model.CategoryModel;
import com.apolorossi.marketplaceapp.rest.manager.CategoriesManager;

import java.util.List;

public class CategoriesPresenter implements CategoriesActivityContract.IPresenter {

    private CategoriesActivityContract.IView mView;

    public CategoriesPresenter(CategoriesActivityContract.IView view){
        mView = view;
    }

    @Override
    public void onViewReady(String id) {

        CategoriesManager categoriesManager = new CategoriesManager();

        categoriesManager.fetch(new CategoriesManager.OnCategoryCallback() {
            @Override
            public void onSuccess(List<CategoryModel> categoriesList) {
               mView.onFetchList(categoriesList);
            }

            @Override
            public void onFailure() {

            }
        }, Integer.parseInt(id));

    }
}
